package com.ko.jsf.listandtabledemo;

import javax.faces.bean.ManagedBean;

@ManagedBean
public class Product {

	// fields
	private String name;
	private String code;

	// no-arg constructor
	public Product() {

	}

	// constructor
	public Product(String name, String code) {
		super();
		this.name = name;
		this.code = code;
		System.out.println(name + code);
	}

	// getters and setters
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

}
