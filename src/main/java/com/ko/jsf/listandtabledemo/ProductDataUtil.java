package com.ko.jsf.listandtabledemo;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

@ManagedBean
@ApplicationScoped
public class ProductDataUtil {

	// list of products
	private List<Product> products;

	// constructor
	public ProductDataUtil() {
		loadSampleData();
	}

	// pro-populate with products
	public void loadSampleData() {
		products = new ArrayList<>();

		products.add(new Product("Green Tea", "KO1"));
		products.add(new Product("Carlos shirt", "KO2"));
	}

	// getter
	public List<Product> getProducts() {
		return products;
	}

}
